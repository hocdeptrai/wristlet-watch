﻿using Microsoft.AspNetCore.Mvc;

namespace ShopDongHoSangTrong.Areas.Admin.Controllers
{
    
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
